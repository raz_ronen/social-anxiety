import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule, ErrorStateMatcher, ShowOnDirtyErrorStateMatcher, MatInputModule, MatFormFieldModule, MatCardModule, MatSelectModule} from '@angular/material';
import {MatCheckboxModule} from '@angular/material/checkbox'

import { HttpClientModule } from '@angular/common/http';
import { SelectionComponent } from './selection/selection.component';

import {LogMonitorModule} from 'ngx-log-monitor';

@NgModule({
  declarations: [
    AppComponent,
    SelectionComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule, 
    MatCheckboxModule,
    HttpClientModule,
    MatInputModule,
    MatFormFieldModule,
    MatCardModule,
    LogMonitorModule,
    MatSelectModule
  ],
  providers: [
    {provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
