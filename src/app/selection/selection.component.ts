import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import * as _ from 'lodash';
import { Algo } from 'src/models/algos';

@Component({
  selector: 'app-selection',
  templateUrl: './selection.component.html',
  styleUrls: ['./selection.component.css']
})
export class SelectionComponent implements OnInit {
  public labels: {name:string, value:string, checked:boolean, show:boolean}[];
  public features: {name:string, value:string, checked:boolean, show: boolean}[];
  iterations: number[];

  @Output() onSelected = new EventEmitter<any>();
  numOfIteration: any;
  
  @Input()
  public set data(data: any) {
    if (data == undefined) return;

    const gen = (obj) => {
      return obj.map((key, index) => {
        return {name: key, value: index, checked: false, show: true}
      });
    }
    this.labels = gen(Object.keys(data));
    let obj: any = Object.entries(data).map(entry => entry[1][0].map(entry => entry[0]))
    this.features =  gen(_.union(obj.flat())).filter(entry => !entry.name.startsWith("Z"));
  }
  
  constructor() { }

  ngOnInit() {
    this.iterations = Array(100).fill(0).map((x,i)=>i+1);
  }

  labelChanged(ev) {
    this.labels.forEach(label=> {
      if (label.name.toLowerCase().includes(ev.target.value.toLowerCase())) {
        label.show = true;
      } else {
        label.show = false;
      }
    })
  }

  passTenLabels(label) {
    return this.labels.filter(label => label.show).includes(label);
  }

  passTenFeatures(feature) {
    return this.features.filter(feature => feature.show).includes(feature);
  }

  featureChanged(ev) {
    this.features.forEach(feature=> {
      if (feature.name.toLowerCase().includes(ev.target.value.toLowerCase())) {
        feature.show = true;
      } else {
        feature.show = false;
      }
    })
  }

  selectKnn() {
    this.select(Algo.KNN);
  }

  selectNeuralNet() {
    this.select(Algo.NEURAL_NET);
  }

  select(algo: Algo) {
    this.onSelected.emit({
      algo: algo,
      labels: this.labels.filter(label => label.checked).map(label => label.name),
      features:  this.features.filter(feature => feature.checked).map(feature => feature.name),
      numOfIteration: this.numOfIteration
    })
  }

}
