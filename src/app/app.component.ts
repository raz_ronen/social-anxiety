import { Component } from '@angular/core';
import {BrowserModule, DomSanitizer, SafeUrl} from '@angular/platform-browser'
import { KnnService, ChosenClassifier, KnnResult } from 'src/services/knn.service';
import { ExcelReaderService } from 'src/services/excel-reader.service';
import { OrExcelTransformerService } from 'src/services/or-excel-transformer.service';
import { Entry } from 'src/models/entry';
import * as tf from '@tensorflow/tfjs';
import { timer, Observable, Operator } from 'rxjs';
import { map, take } from 'rxjs/operators';
import {LogMessage as NgxLogMessage, LogMessage} from 'ngx-log-monitor';
import { Algo } from 'src/models/algos';
import { analyzeNgModules } from '@angular/compiler';
import { NeuralNetService } from 'src/services/neural-net.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  data: any;
  onSelection: boolean = false;
  onRunComplete:boolean = false;
  onChosenFile: boolean = false;
  downloadDataUri: SafeUrl;
  downloadTrainingSetUrl: SafeUrl;
  downloadTestSetUri: SafeUrl;
  chosenClassifier: ChosenClassifier;
  downloadJsonHref: any;
  listOfRatios: {num: number, value: string}[];

  logs: NgxLogMessage[] = [
    {message: 'A simple log message'},
    // {message: 'A success message', type: 'SUCCESS'},
    // {message: 'A warning message', type: 'WARN'},
    // {message: 'An error message', type: 'ERR'},
    // {message: 'An info message', type: 'INFO'},
    // {message: 'A simple log message'},
    // {message: 'A success message', type: 'SUCCESS'},
    // {message: 'A warning message', type: 'WARN'},
    // {message: 'An error message', type: 'ERR'},
    // {message: 'An info message', type: 'INFO'},
    // {message: 'A simple log message'},
    // {message: 'A success message', type: 'SUCCESS'},
    // {message: 'A warning message', type: 'WARN'},
    // {message: 'An error message', type: 'ERR'},
    // {message: 'An info message', type: 'INFO'},
  ];
  chosensuccRatioDisplay: string;
  chosenK: number;

  // logStream$ = new Observable<LogMessage>(subscriber => {
  //   subscriber.next(this.logs[0]);
  // }).sub;

  constructor(private knnService: KnnService,
    private neuralNetService: NeuralNetService,
    private excelReaderService: ExcelReaderService,
    private orExcelTransformerService: OrExcelTransformerService,
    private sanitizer: DomSanitizer) { 
  }

  ngOnInit(): void {
    //Called after the constructor, initializing input properties, and the first call to ngOnChanges.
    //Add 'implements OnInit' to the class.
    this.onChosenFile = true;
  }

  fileChanged(ev) {
    this.onSelection = true;
    this.onChosenFile = false;
    this.onRunComplete = false;
    this.excelReaderService.importFromExcel(ev)
    .subscribe((response: any[]): void => {
      this.data = this.orExcelTransformerService.transform(response);
    });
  }

  onSelected(selection) {
    this.onSelection = false;
    const examples: Entry[] = [];
    const idColumn = "subject_id";

    Object.entries(this.data).forEach((group: [string, any[]]) => {
      const groupName = group[0];

      if (selection.labels.includes(groupName)) {
        group[1].forEach((example: []) => {
          const id = example.filter(col => col[0] == idColumn)[0][1]
          let features: any[] = example.filter(feature => selection.features.includes(feature[0]));

          features = features.sort(this.Comparator);
          features = features.map(feature => typeof feature[1] == "number" ? feature[1] : 0);

          examples.push({
            id: id,
            features: features,
            group: groupName
          })
        });
      }
    })

    if (selection.algo == Algo.KNN) {
      this.executeKnn(selection, examples);
    } else if (selection.algo == Algo.NEURAL_NET) {
      this.executeNeuralNet(selection, examples);
    }
  }

  executeNeuralNet(selection, examples) {
    this.neuralNetService.runNeuralNet(examples, selection.labels).then((res) => {
      console.log(res);
    })
  }

  executeKnn(selection, examples) {
    if (selection.numOfIteration == 1) {
      this.knnService.runKnn(examples).then((result: KnnResult) => {
        this.displayOneKnnResult(result);
      })
    } else {
      this.knnService.runKnnMultipleTimes(examples, selection.numOfIteration).then((result: KnnResult[]) => {
        const successRatios: number[] = result.map(knnResult => knnResult.chosenClassifier.successRatio);
        this.listOfRatios = successRatios.map((successRatio, num) => {
          return {
            num: num, 
            value: (successRatio * 100).toFixed(2) + "%" 
          }
        });

        const average = successRatios.reduce((a,b) => a + b,0) / successRatios.length;

        this.chosensuccRatioDisplay = (average * 100).toFixed(2) + "%";

        this.onRunComplete = true;
      });
    } 
  }

  displayOneKnnResult(result: KnnResult) {
    const { chosenClassifier, trainSet, testSet } = result;

    this.downloadDataUri = this.provideDownlowdUri(this.data);
    this.downloadTrainingSetUrl = this.provideDownlowdUri(trainSet.map(entry => entry.id));
    this.downloadTestSetUri = this.provideDownlowdUri(testSet.map(entry => entry.id));

    this.chosensuccRatioDisplay = chosenClassifier.succRatioDisplay
    this.chosenK = chosenClassifier.k;

    this.chosenClassifier = chosenClassifier;
    this.onRunComplete = true;
  }

  provideDownlowdUri(object): SafeUrl {
    var theJSON = JSON.stringify(object);
    return this.sanitizer.bypassSecurityTrustUrl("data:text/json;charset=UTF-8," + encodeURIComponent(theJSON));
  }

  Comparator(a, b) {
    if (a[0] < b[0]) return -1;
    if (a[0] > b[0]) return 1;
    return 0;
  }
}
