export class Candidate {
    id: number;
    height: number;
    age: number;
    weight: number;
    group: number;
}