import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrExcelTransformerService {

  constructor() { }

  public transform(table: any): any {
    table = table.map(row => {
        return Object.entries(row).filter(entry => {
          return /subject|neutral|command|request/.test(entry[0])
        })
      }
    );

    // Table that every subjectId has 6 rows.
    let groups = {};
    table.forEach(row => {
      this.joinGroup(groups, row, "neutral");
      this.joinGroup(groups, row, "command");
      this.joinGroup(groups, row, "request");
      this.joinGroup(groups, row, "neutral1");
      this.joinGroup(groups, row, "command1");
      this.joinGroup(groups, row, "request1");
    });

    return groups;
  } 

  joinGroup(groups, row, gName) {
    if (groups[gName] == undefined) {
      groups[gName] = []
    }
    groups[gName].push(row.filter(entry => new RegExp("subject|" + gName + "$").test(entry[0])).map(entry => {
      return [ entry[0].replace("." + gName,"").replace("_objects","").replace("_sentences",""), entry[1] ];
  }))
  }
}
