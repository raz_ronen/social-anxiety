import { Injectable } from '@angular/core';
import { Entry } from '../models/entry';

export type Set = Entry[];

@Injectable({
  providedIn: 'root'
})
export class UtilitiesService {

  constructor() { }

  getRandomSetsByRatio(examples: Set, ratio: number): { trainSet: Set, testSet: Set } {
    console.log(`Generating random training and test set by ratio: ${ratio} (training).`)
    const trainSetLength = (ratio / 100.0) * examples.length;
    examples = this.shuffle(examples);

    const sets = {
      trainSet: examples.slice(0, trainSetLength),
      testSet: examples.slice(trainSetLength)
    };

    console.log("Sets genereted.")
    return sets;
  }

   /**
 * Shuffles array in place. ES6 version
 * @param {Array} a items An array containing the items.
 */
shuffle(a) {
  for (let i = a.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [a[i], a[j]] = [a[j], a[i]];
  }
  return a;
}
}
