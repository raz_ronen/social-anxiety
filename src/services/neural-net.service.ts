import { Injectable } from '@angular/core';
import { Set, UtilitiesService } from '../services/utilities.service';
import * as tf from '@tensorflow/tfjs';

export type NeuralNetResult = {

}

@Injectable({
  providedIn: 'root'
})
export class NeuralNetService {
  ratio: number = 80.0;

  constructor(private utilitiesServices: UtilitiesService) { }

  async runNeuralNet(examples: Set, labels: string[]) {
    if (examples.length == 0) {
      throw new Error("Empty examples set.");
    }

    const { trainSet, testSet } = this.utilitiesServices.getRandomSetsByRatio(examples, this.ratio);

    // convert data
    const trainingData: tf.Tensor<tf.Rank.R2> = tf.tensor2d(trainSet.map(entry => entry.features));
    const testingData: tf.Tensor<tf.Rank.R2> = tf.tensor2d(testSet.map(entry => entry.features));
    const traingingOutputData: tf.Tensor<tf.Rank.R2> = tf.tensor2d(trainSet.map(entry => {
      return [...labels.map(label => label == entry.group ? 1 : 0)]
      }
    ));
    

    // creating model
    const model = tf.sequential();

    // Input layer:
    model.add(tf.layers.dense({
      inputShape: [trainSet[0].features.length],
      activation: "sigmoid",
      units: 10
    }));

    // Output layer:
    model.add(tf.layers.dense({
      inputShape: [labels.length],
      activation: "softmax",
      units: labels.length
    }));

    // compiling model

    model.compile({
      loss: "categoricalCrossentropy",
      optimizer: tf.train.adam()
    })

    // predicting model

    async function train_data(){
      for(let i=0;i<40;i++){
        const res = await model.fit(trainingData,
            traingingOutputData,{epochs: trainSet.length});  
        console.log(`Iteration ${i}: ${res.history.loss[i]}`);          
      }
    }

    await train_data();

    let predictions: any = model.predict(testingData);

    predictions.print();
    return predictions;
  }
}
