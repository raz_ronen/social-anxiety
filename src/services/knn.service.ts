import { Injectable } from '@angular/core';
import { Candidate } from '../models/candidate';
import * as tf from '@tensorflow/tfjs';
import * as knnClassifier from '@tensorflow-models/knn-classifier';
import { Entry } from '../models/entry';
import { Set, UtilitiesService } from '../services/utilities.service';

export type KnnResult = {
  chosenClassifier: ChosenClassifier,
  trainSet: Set,
  testSet: Set,
}

export type ChosenClassifier = {
  classifier: knnClassifier.KNNClassifier,
  k: number,
  successRatio: number,
  succRatioDisplay: string
}

@Injectable({
  providedIn: 'root'
})
export class KnnService {
  knnClassifier: any;
  featureExtractor: any;
  candidates: Candidate[];

  ratio: number = 80.0;

  constructor(private utilitiesServices: UtilitiesService) { }

  runKnnMultipleTimes(examples: Set, numOfIteration: number): Promise<KnnResult[]> {
    const allPromises: Promise<KnnResult>[] = [];
    for (let i = 0; i < numOfIteration; i++) {
      console.log(`Starting iteration: ${i}`);
      allPromises.push(this.runKnn(examples))
    }

    return Promise.all(allPromises).then((value: KnnResult[]) => {
      return value;
    });
  }

  runKnn(examples: Set): Promise<KnnResult> {
    const { trainSet, testSet } = this.utilitiesServices.getRandomSetsByRatio(examples, this.ratio);

    console.log("Creating classifier.");
    const classifier: knnClassifier.KNNClassifier = knnClassifier.create();
    const groups: string[] = [];

    trainSet.forEach((example: Entry) => {
      const group: number = groups.includes(example.group) ? groups.indexOf(example.group) : groups.push(example.group) -1;

      classifier.addExample(tf.tensor(example.features), group);
    });

    console.log("Creation completed.");

    let chosenClassifier: ChosenClassifier = <ChosenClassifier>{
      successRatio: 0
    }
    const allPredictions: Promise<void>[] = [];

    for (let k = 2; k < 30 && k < examples.length; k++) {
      const prediction: Promise<boolean>[] = [];

      testSet.forEach((example: Entry) => {
        const group: number = groups.includes(example.group) ? groups.indexOf(example.group) : -1;

        if (group == -1) {
          return;
        }

        prediction.push(classifier.predictClass(tf.tensor(example.features), k).then((result) => {
          return group == result.classIndex;
        }));
      });

      allPredictions.push(Promise.all(prediction).then(preds => {
        let successRatio = preds.filter(res => res).length / preds.length;
        let succRatioDisplay: string = (successRatio * 100).toFixed(2) + "%";

        if (chosenClassifier.successRatio < successRatio) {
          chosenClassifier = <ChosenClassifier> {
            classifier,
            k,
            successRatio,
            succRatioDisplay,
          }
        }

        console.log(`K: ${k}: Finish test set prediction with: ${succRatioDisplay}`);
      }));
    }
    
    return Promise.all(allPredictions).then(() => {
      console.log("Done.\n")

      console.log(`Highest prediction for K = ${chosenClassifier.k} with ${chosenClassifier.succRatioDisplay} success ratio.`);
      return { chosenClassifier, trainSet, testSet };
    })
  }
}
