This repo is a site for social anxiety research.
It's an Angular application with TensorFlow.js AI algorithms. 
Currntly supports Knn, Neural Network is under development.

How to run this site on your local computer:
1. Download Node.js: https://nodejs.org/en/download/
2. Download git bash: https://git-scm.com/downloads
3. Open git bash and cd to a free directory.
4. run 'git clone https://raz_ronen@bitbucket.org/raz_ronen/social-anxiety.git'
5. run 'cd social-anxiety'
6. run 'npm install'
7. run 'ng serve --open'


How to use site?
1. click 'Choose file' and chose the DATA_SET.csv from the root directory
2. Choose the labels/features you want and select 1 or multiple run of Knn iterations.
3. Click 'Run'